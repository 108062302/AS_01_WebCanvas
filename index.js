const canvas = document.getElementById("canvas");
canvas.width = window.innerWidth - 100;
canvas.height = 400;

let context = canvas.getContext("2d");
let new_context = canvas.getContext("2d");
let draw_color = "black";
let draw_width = "2";
let drawing = false;

var mode = "pen";

var startX;
var startY;

var hasInput = false;
var font = '8px sans-serif';
var size = '8px';
var fontstyle = 'sans-serif';

let restore_array = [];
let index = -1;

canvas.addEventListener("touchstart", start, false);
canvas.addEventListener("touchmove", draw, false);
canvas.addEventListener("mousedown", start, false);
canvas.addEventListener("mousemove", draw, false);
canvas.addEventListener("touchend", stop, false);
canvas.addEventListener("mouseup", stop, false);
canvas.addEventListener("mouseout", stop, false);

canvas.onclick = function(e) {
    if (hasInput) return;
    mouseX = parseInt(e.clientX - canvas.offsetLeft);
    mouseY = parseInt(e.clientY - canvas.offsetTop);
    if(mode == "word") {
        addInput(mouseX, mouseY);        
    }
}

function change_size() {
    var val = document.getElementById("sizeselect").value;
    context.font = val + 'px ' + fontstyle ;
    size = val + 'px';
}

function change_font() {
    var val = document.getElementById("fontselect").value;
    context.font = size + " " + val;
    fontstyle = val;
}

function addInput(x, y) {
    var input = document.createElement('input');

    input.type = 'text';
    input.style.position = 'fixed';
    input.style.left = x + 'px';
    input.style.top = y + 'px';

    input.onkeydown = handleEnter;

    document.body.appendChild(input);

    input.focus();
    
    hasInput = true;
}

function handleEnter(e) {
    var keyCode = e.keyCode;
    if (keyCode === 13) {
        drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
        document.body.removeChild(this);
        hasInput = false;
    }
}

function drawText(txt, x, y) {
    context.textBaseline = 'top';
    context.textAlign = 'left';
    context.fillText(txt, x - canvas.offsetLeft, y - canvas.offsetTop);
    index += 1;
    restore_array.splice(index, 0, context.getImageData(0, 0, canvas.width, canvas.height));
    console.log(index);
}

function change_color(e) {
    draw_color = e.style.background;
}

function start(e) {
    drawing = true;
    context.beginPath();
    context.moveTo(e.clientX - canvas.offsetLeft, e.clientY - canvas.offsetTop);

    startX = parseInt(e.clientX - canvas.offsetLeft);
    startY = parseInt(e.clientY - canvas.offsetTop);
    e.preventDefault();

}


function draw(e) {
    e.preventDefault();
    e.stopPropagation();

    if(drawing) {
        if(mode == "pen") {
            context.lineTo(e.clientX - canvas.offsetLeft, e.clientY - canvas.offsetTop);
            context.strokeStyle = draw_color;
            context.lineWidth = draw_width;
            context.lineCap = "round";
            context.lineJoin = "round";
            context.stroke();
        }
        else if(mode == "eraser") {
            context.lineTo(e.clientX - canvas.offsetLeft, e.clientY - canvas.offsetTop);
            context.strokeStyle = ("rgba(0, 0, 0, 1.0)");
            context.lineWidth = draw_width;
            context.lineCap = "round";
            context.lineJoin = "round";
            context.stroke();  
        }
        else if(mode == "full_rect") {
            mouseX = parseInt(e.clientX - canvas.offsetLeft);
            mouseY = parseInt(e.clientY - canvas.offsetTop);
            var width = mouseX - startX;
            var height = mouseY - startY;
            
            context.clearRect(0, 0, canvas.width, canvas.height);
            if(index != -1) {
                context.putImageData(restore_array[index], 0, 0);
            }

            context.lineWidth = draw_width;
            context.strokeStyle = draw_color;

            context.strokeRect(startX, startY, width, height);
        }
        else if(mode == "circle") {
            mouseX = parseInt(e.clientX - canvas.offsetLeft);
            mouseY = parseInt(e.clientY - canvas.offsetTop);

            draw_circle(mouseX, mouseY);
        }
        else if(mode == "triangle") {
            mouseX = parseInt(e.clientX - canvas.offsetLeft);
            mouseY = parseInt(e.clientY - canvas.offsetTop);

            draw_triangle(mouseX, mouseY); 
        }
    }
    e.preventDefault();
}

function stop(e) {
    if(drawing) {
        context.closePath();
        drawing = false;
        hasInput = false;
    }
    else {
        return ;
    }
    e.preventDefault();

    if(e.type != "mouseout" && mode != "word") {
        index += 1;
        restore_array.splice(index, 0, context.getImageData(0, 0, canvas.width, canvas.height));//insert function
    }
}

function clear_canvas() {
    context.fillStyle = ("rgba(0, 0, 0, 1.0)");
    context.globalCompositeOperation = "destination-out";
    context.fillRect(0, 0, canvas.width, canvas.height);
    
    restore_array = [];
    index = -1;
    context.globalCompositeOperation = "source-out";
    change_pencil();
}

function undo_last() {
    if(index <= 0) {
        clear_canvas();
        index = -1;
    }
    else {
        index -= 1;
        context.putImageData(restore_array[index], 0, 0);
    }
}

function redo_last() {
    if(index < 0) {
        clear_canvas();
        index = -1;
    }
    else {
        index += 1;
        context.putImageData(restore_array[index], 0, 0);
    }
}

function change_pencil() {
    mode = "pen";
    context.globalCompositeOperation = "source-over";
    document.body.style.cursor="url('pencil.png'),auto";
}

function change_eraser() {
    mode = "eraser";
    context.globalCompositeOperation = "destination-out";
    document.body.style.cursor="url('eraser.png'),auto";
}

function change_rect() {
    mode = "full_rect";
    context.globalCompositeOperation = "source-over";
    document.body.style.cursor="url('square.png'),auto";
}

function change_circle() {
    mode = "circle";
    context.globalCompositeOperation = "source-over";
    document.body.style.cursor="url('circle.png'),auto";
}

function change_triangle() {
    mode = "triangle";
    context.globalCompositeOperation = "source-over";
    document.body.style.cursor="url('triangle.png'),auto";
}

function change_word() {
    mode = "word";
    context.globalCompositeOperation = "source-over";
    document.body.style.cursor="url('text.png'),auto";
}

function download_image() {
    let downloadLink = document.createElement('a');
    downloadLink.setAttribute('download', 'image.png');
    let canvas = document.getElementById('canvas');
    let dataURL = canvas.toDataURL('image/png');
    let url = dataURL.replace(/^data:image\/png/,'data:application/octet-stream');
    downloadLink.setAttribute('href', url);
    downloadLink.click();
}

function draw_circle(x, y) {
    var radius = (Math.sqrt((x-startX)*(x-startX) + (y-startY)*(y-startY))) / 2;

    context.clearRect(0, 0, canvas.width, canvas.height);
    if(index != -1) {
        context.putImageData(restore_array[index], 0, 0);
    }

    var centerX = (x + startX) / 2;
    var centerY = (y + startY) / 2;

    context.beginPath();
    context.arc(centerX, centerY, radius, 0, Math.PI * 2, true);
    context.strokeStyle = draw_color;
    context.lineWidth = draw_width;
    context.stroke();
}

function draw_triangle(x, y) {
    context.clearRect(0, 0, canvas.width, canvas.height);
    if(index != -1) {
        context.putImageData(restore_array[index], 0, 0);
    }

    context.beginPath();
    context.lineTo((x + startX) / 2, startY);
    context.strokeStyle = draw_color;
    context.lineWidth = draw_width;
    
    context.lineTo(x, y);
    context.lineTo(startX, y);
    context.lineTo((x + startX) / 2, startY);
    context.stroke();
}

let upload = document.getElementById('upload');

upload.addEventListener('change', function(e) {
    clear_canvas();
    
    if(e.target.files) {
        let imageFile = e.target.files[0]; //here we get the image file
        var reader = new FileReader();
        reader.readAsDataURL(imageFile);
        reader.onloadend = function (e) {
            var myImage = new Image(); // Creates image object
            myImage.src = e.target.result; // Assigns converted image to image object
            myImage.onload = function(ev) {
            context.drawImage(myImage,0,0); // Draws the image on canvas
            let imgData = canvas.toDataURL("image/png",0.75); // Assigns image base64 string in jpeg format to a variable
            index += 1;
            restore_array.splice(index, 0, context.getImageData(0, 0, canvas.width, canvas.height));//insert function
        }
      }
    }
  });
