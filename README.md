# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

![](https://i.imgur.com/ajgyJvD.png)
    
##### On the bottom right part of the page, we can see many icons. The first one is absolutely the pencil. Once we click it, we can see if the cursor changes into a pencil's image to check if the mode is pencil. Then we can draw on the canvas.
##### The second icon is the eraser, it is also very easy, choose it and check if the cursor change then use it.
##### The third one is the text input, click wherever you want to input the text, then type in the box whatever you want.
##### The other three shapes are for drawing the shapes. The square one is to draw rectangle, circle for circle, and triangle for triangle.
##### The color and width of mentioned above can be changed by clicking the black block and the pen-range on the middle of the bottom of the page.
##### On the left of the bottom, there are four buttons and we can see the description inside the box. Re/Undo is just to go back or go forward the step before or after. Clear is to clear the whole canvas. Download is to download the current canvas as a png. 選擇檔案 is to upload an image you choose.
##### These are how to use my canvas.


### Function description

##### For drawing on the canvas, I use many addeventlistener to see what I do on the canvas such as 'mouseup' or 'mousedown' to see if I click the mouse or 'mousemove' to see if I am dragging. By adding function inside the eventlistener, I can start drawing. While mousedown, I begin a path and move to where I click the function. Then I check the mode now. Except the text input and eraser, I use stroke() function in other mode to draw the line of what I draw or the shape.
##### One event function is start which I calculate the mouse direction on the canvas or begin the path. Another function is draw for dragging the mouse. I stroke what I draw to see if the shape is what I want. I always clear the canvas and put the last image I restore from the canvas to prevent from many same shape on the canvas at a time.
##### I use document.createElement('input') to create a box of text input on the canvas. Once I change the mode to "word", I click the canvas and I will call addInput(x, y) where x and y is where I click the canvas. I have a handler to handle whether I press Enter or not. Then I input the value I type to another function drawText() then it calls canvas.filltext(). Then we can see what we type on the canvas.
##### To choose the font, I add to select element in my html. Then I add many options to choose the font size and the style. Then I get the value by document.getElementId().value to get the value in the select element. 
##### To download the canvas image, I create a whatever element to setAttribute to download. Then I call canvas.toDataURL() where I put what I want to download inside the block. Then I call the rest funtion to transform the canvas image to a png file.
##### To upload an image, I create an input element. I call an evenlistener with the type 'change'. The event function first get the image file in the target.files. Then I create a new FileReader and read the image file. Then I convert image to image object and draw it on the canvas.
##### The color-selector and the pen-range are also input element. Then I can get the value in the .js and assign it to the context.linewidth and strokestyle.

### Gitlab page link

    "https://108062302.gitlab.io/AS_01_WebCanvas"

### Others (Optional)

    It is so difficult...

<style>
table th{
    width: 100%;
}
</style>
